# Update K8s cara susah

![my slide](img/Screenshot_2022-05-19_at_9.08.15_AM.png)

Upgrade K8s cara susah adalah sesi untuk belajar bagaimana K8s diupgradekan bagi ujian CKA. 
Sesi kali ini merupakan walkthrough/hands-on dengan menggunakan aplikasi [kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/)

Rujukan Utama

[Taman Permainan K8s oleh Katacoda](https://www.katacoda.com/courses/kubernetes/playground)

[Upgrade K8s dengan kubeadm ](https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/)


## Upgrade Control Plane atau Master Node

1. Lancarkan K8s
```sh
launch.sh
```

2. Tunggu sehingga nod-nod sedia untuk bekerja dan pastikan versi K8s
```sh
watch -n 0.5 kubectl get node
```
* k8s = 1.18

3. Pastikan OS jenis apa 
```sh
uname -a
```
* O.S = ubuntu

4. Kita dah tau K8s 1.18 dan OS ialah ubuntu. Oleh itu kita larikan command yang berikut
```sh
apt update && apt-cache madison kubeadm | grep 1.19
```

5. Install kubeadm 
```sh
apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=1.19.3-00 && \
apt-mark hold kubeadm
```

6. Check kubeadm version
```sh
kubeadm version
```
* kubeadm sepatunya sudah 1.19.3

7. Check K8s version
```sh
kubeadm upgrade plan 
```

8. Upgrade k8s !!!
```sh
kubeadm upgrade apply v1.19.3 -y
```

9. Check K8s version
```sh
kubeadm upgrade plan 
```
* sepatunya sudah versi 1.19.3

10. Persediaan untuk upgrade kubelet. Keluarkan semua pods dari **controlplane**
```sh
kubectl drain controlplane --ignore-daemonsets
```

11. upgrade kubelet dan kubectl
```sh
apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet=1.19.3-00 kubectl=1.19.3-00 && \
apt-mark hold kubelet kubectl
```

12. Restart servis kubelet
```sh
systemctl daemon-reload && sudo systemctl restart kubelet
```

13. check versi kubelet
```sh
kubectl get nodes
```
* versi controlplane sepatunya 1.19.3

14. check versi kubectl
```sh 
kubectl version
```
* versi kubectl sepatunya 1.19.3


15. Uncordon controlplane
```sh
kubectl uncordon controlplane
```


## Upgrade Worker atau Worker Node
1. upgrade kubeadm 
```sh
apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=1.19.3-00 && \
apt-mark hold kubeadm
```

2. Dapatkan config yang terkini
```sh
kubeadm upgrade node
```

3. Kepung node01 daripada kemasukan pods . **INI DILAKUKAN PADA controlplane**
```sh
kubectl drain node01 --ignore-daemonsets
```

4. Upgrade kubelet dan kubectl
```sh
apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet=1.19.3-00 kubectl=1.19.3-00 && \
apt-mark hold kubelet kubectl
```

5. Restart servis kubelet
```sh
systemctl daemon-reload && sudo systemctl restart kubelet
```

6. check versi kubelet **INI DILAKUKAN PADA controlplane**
```sh
kubectl get nodes
```
* versi controlplane sepatunya 1.19.3

7. Uncordon controlplane **INI DILAKUKAN PADA controlplane**
```sh
kubectl uncordon node01
```

## Cuba lancarkan nginx X 10

1. Buat deployment nginx dengan 10 replica. **INI DILAKUKAN PADA controlplane**
```sh
kubectl create deployment mynginx --image=nginx --replicas=10
```

2. Pastikan lokasi dimana pod-pod itu dihidupkan
```sh
kubectl get pods -o wide
```
KBAT: Kenapa pod-pod itu hidup disitu?


